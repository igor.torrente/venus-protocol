<?xml version="1.0" encoding="UTF-8"?>
<registry>
    <comment>
Copyright 2020 Google LLC

SPDX-License-Identifier: Apache-2.0 OR MIT
    </comment>

    <comment>
This file, VK_MESA_venus_protocol.xml, provides definitions for
VK_MESA_venus_protocol. These definitions are used by Venus driver and
renderer, and are not exposed to applications.
    </comment>

    <types>
        <type requires="VkCommandStreamExecutionFlagBitsMESA" category="bitmask">typedef <type>VkFlags</type> <name>VkCommandStreamExecutionFlagsMESA</name>;</type>
        <type requires="VkRingCreateFlagBitsMESA" category="bitmask">typedef <type>VkFlags</type> <name>VkRingCreateFlagsMESA</name>;</type>
        <type requires="VkRingNotifyFlagBitsMESA" category="bitmask">typedef <type>VkFlags</type> <name>VkRingNotifyFlagsMESA</name>;</type>

        <type name="VkCommandStreamExecutionFlagBitsMESA" category="enum"/>
        <type name="VkRingCreateFlagBitsMESA" category="enum"/>
        <type name="VkRingNotifyFlagBitsMESA" category="enum"/>

        <type category="struct" name="VkCommandStreamDescriptionMESA">
            <member><type>uint32_t</type>                         <name>resourceId</name></member>
            <member><type>size_t</type>                           <name>offset</name></member>
            <member><type>size_t</type>                           <name>size</name></member>
        </type>
        <type category="struct" name="VkCommandStreamDependencyMESA">
            <member><type>uint32_t</type>                         <name>srcCommandStream</name></member>
            <member><type>uint32_t</type>                         <name>dstCommandStream</name></member>
        </type>

        <type category="struct" name="VkRingCreateInfoMESA">
            <member values="VK_STRUCTURE_TYPE_RING_CREATE_INFO_MESA"><type>VkStructureType</type> <name>sType</name></member>
            <member optional="true">const <type>void</type>*      <name>pNext</name></member>
            <member optional="true"><type>VkRingCreateFlagsMESA</type> <name>flags</name></member>
            <member><type>uint32_t</type>                         <name>resourceId</name></member>
            <member><type>size_t</type>                           <name>offset</name></member>
            <member><type>size_t</type>                           <name>size</name></member>
            <member><type>uint64_t</type>                         <name>idleTimeout</name></member>
            <member><type>size_t</type>                           <name>headOffset</name></member>
            <member><type>size_t</type>                           <name>tailOffset</name></member>
            <member><type>size_t</type>                           <name>statusOffset</name></member>
            <member><type>size_t</type>                           <name>bufferOffset</name></member>
            <member><type>size_t</type>                           <name>bufferSize</name></member>
            <member><type>size_t</type>                           <name>extraOffset</name></member>
            <member><type>size_t</type>                           <name>extraSize</name></member>
        </type>

        <type category="struct" name="VkMemoryResourcePropertiesMESA">
            <member values="VK_STRUCTURE_TYPE_MEMORY_RESOURCE_PROPERTIES_MESA"><type>VkStructureType</type> <name>sType</name></member>
            <member optional="true"><type>void</type>*            <name>pNext</name></member>
            <member><type>uint32_t</type>                         <name>memoryTypeBits</name></member>
        </type>

        <type category="struct" name="VkImportMemoryResourceInfoMESA" structextends="VkMemoryAllocateInfo">
            <member values="VK_STRUCTURE_TYPE_IMPORT_MEMORY_RESOURCE_INFO_MESA"><type>VkStructureType</type> <name>sType</name></member>
            <member optional="true">const <type>void</type>*      <name>pNext</name></member>
            <member><type>uint32_t</type>                         <name>resourceId</name></member>
        </type>

        <type category="struct" name="VkVenusExperimentalFeatures100000MESA">
            <member><type>VkBool32</type>                         <name>memoryResourceAllocationSize</name></member>
            <member><type>VkBool32</type>                         <name>globalFencing</name></member>
            <member><type>VkBool32</type>                         <name>largeRing</name></member>
        </type>

        <type category="struct" name="VkMemoryResourceAllocationSizeProperties100000MESA" structextends="VkMemoryResourcePropertiesMESA">
            <member values="VK_STRUCTURE_TYPE_MEMORY_RESOURCE_ALLOCATION_SIZE_PROPERTIES_100000_MESA"><type>VkStructureType</type> <name>sType</name></member>
            <member optional="true"><type>void</type>*            <name>pNext</name></member>
            <member><type>uint64_t</type>                         <name>allocationSize</name></member>
        </type>
    </types>

    <enums name="VkCommandStreamExecutionFlagBitsMESA" type="bitmask">
    </enums>
    <enums name="VkRingCreateFlagBitsMESA" type="bitmask">
    </enums>
    <enums name="VkRingNotifyFlagBitsMESA" type="bitmask">
    </enums>

    <commands>
        <command>
            <proto><type>void</type> <name>vkSetReplyCommandStreamMESA</name></proto>
            <param>const <type>VkCommandStreamDescriptionMESA</type>* <name>pStream</name></param>
        </command>
        <command>
            <proto><type>void</type> <name>vkSeekReplyCommandStreamMESA</name></proto>
            <param><type>size_t</type> <name>position</name></param>
        </command>

        <command>
            <proto><type>void</type> <name>vkExecuteCommandStreamsMESA</name></proto>
            <param><type>uint32_t</type> <name>streamCount</name></param>
            <param len="streamCount">const <type>VkCommandStreamDescriptionMESA</type>* <name>pStreams</name></param>
            <param len="streamCount" optional="true">const <type>size_t</type>* <name>pReplyPositions</name></param>
            <param optional="true"><type>uint32_t</type> <name>dependencyCount</name></param>
            <param len="dependencyCount">const <type>VkCommandStreamDependencyMESA</type>* <name>pDependencies</name></param>
            <param optional="true"><type>VkCommandStreamExecutionFlagsMESA</type> <name>flags</name></param>
        </command>

        <command>
            <proto><type>void</type> <name>vkCreateRingMESA</name></proto>
            <param><type>uint64_t</type><name>ring</name></param>
            <param>const <type>VkRingCreateInfoMESA</type>* <name>pCreateInfo</name></param>
        </command>
        <command>
            <proto><type>void</type> <name>vkDestroyRingMESA</name></proto>
            <param><type>uint64_t</type> <name>ring</name></param>
        </command>
        <command>
            <proto><type>void</type> <name>vkNotifyRingMESA</name></proto>
            <param><type>uint64_t</type> <name>ring</name></param>
            <param><type>uint32_t</type> <name>seqno</name></param>
            <param optional="true"><type>VkRingNotifyFlagsMESA</type> <name>flags</name></param>
        </command>
        <command>
            <proto><type>void</type> <name>vkWriteRingExtraMESA</name></proto>
            <param><type>uint64_t</type> <name>ring</name></param>
            <param><type>size_t</type> <name>offset</name></param>
            <param><type>uint32_t</type> <name>value</name></param>
        </command>
        <command>
            <proto><type>VkResult</type> <name>vkGetMemoryResourcePropertiesMESA</name></proto>
            <param><type>VkDevice</type> <name>device</name></param>
            <param><type>uint32_t</type> <name>resourceId</name></param>
            <param><type>VkMemoryResourcePropertiesMESA</type>* <name>pMemoryResourceProperties</name></param>
        </command>
        <command>
            <proto><type>void</type> <name>vkGetVenusExperimentalFeatureData100000MESA</name></proto>
            <param optional="false,true"><type>size_t</type>* <name>pDataSize</name></param>
            <param optional="true" len="pDataSize"><type>void</type>* <name>pData</name></param>
        </command>
    </commands>

    <extensions>
        <extension name="VK_MESA_venus_protocol" number="385" type="instance" author="MESA" contact="Chia-I Wu @olvaffe1" supported="vulkan">
            <require>
                <enum value="100000"                                        name="VK_MESA_VENUS_PROTOCOL_SPEC_VERSION"/>
                <enum value="&quot;VK_MESA_venus_protocol&quot;"            name="VK_MESA_VENUS_PROTOCOL_EXTENSION_NAME"/>
                <enum offset="0" extends="VkStructureType"                  name="VK_STRUCTURE_TYPE_RING_CREATE_INFO_MESA"/>
                <enum offset="1" extends="VkStructureType"                  name="VK_STRUCTURE_TYPE_MEMORY_RESOURCE_PROPERTIES_MESA"/>
                <enum offset="2" extends="VkStructureType"                  name="VK_STRUCTURE_TYPE_IMPORT_MEMORY_RESOURCE_INFO_MESA"/>
                <enum offset="3" extends="VkStructureType"                  name="VK_STRUCTURE_TYPE_MEMORY_RESOURCE_ALLOCATION_SIZE_PROPERTIES_100000_MESA"/>
                <type name="VkCommandStreamExecutionFlagBitsMESA"/>
                <type name="VkCommandStreamExecutionFlagsMESA"/>
                <type name="VkRingCreateFlagBitsMESA"/>
                <type name="VkRingCreateFlagsMESA"/>
                <type name="VkRingNotifyFlagBitsMESA"/>
                <type name="VkRingNotifyFlagsMESA"/>
                <type name="VkCommandStreamDescriptionMESA"/>
                <type name="VkCommandStreamDependencyMESA"/>
                <type name="VkRingCreateInfoMESA"/>
                <type name="VkMemoryResourcePropertiesMESA"/>
                <type name="VkImportMemoryResourceInfoMESA"/>
                <type name="VkVenusExperimentalFeatures100000MESA"/>
                <type name="VkMemoryResourceAllocationSizeProperties100000MESA"/>
                <command name="vkSetReplyCommandStreamMESA"/>
                <command name="vkSeekReplyCommandStreamMESA"/>
                <command name="vkExecuteCommandStreamsMESA"/>
                <command name="vkCreateRingMESA"/>
                <command name="vkDestroyRingMESA"/>
                <command name="vkNotifyRingMESA"/>
                <command name="vkWriteRingExtraMESA"/>
                <command name="vkGetMemoryResourcePropertiesMESA"/>
                <command name="vkGetVenusExperimentalFeatureData100000MESA"/>
            </require>
        </extension>
    </extensions>
</registry>
